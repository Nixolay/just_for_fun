package justifier

import (
	"fmt"
	"testing"
)

func TestJustify(t *testing.T) {
	type test struct {
		words        []string
		stringLength int
		expectedStr  string
	}

	testCases := []test{
		test{words: []string{"xxx"}, stringLength: 3, expectedStr: "xxx"},     // Одно слово без пробелов
		test{words: []string{"xxx"}, stringLength: 4, expectedStr: "xxx "},    // Одно слово с одним пробелом
		test{words: []string{"xxx"}, stringLength: 7, expectedStr: "xxx    "}, // Одно слово с пробелами

		test{words: []string{"x", "y"}, stringLength: 3, expectedStr: "x y"},               // Два слова, один пробел
		test{words: []string{"x", "y", "z"}, stringLength: 5, expectedStr: "x y z"},        // Три слова, по одному пробелу
		test{words: []string{"x", "y", "z", "w"}, stringLength: 7, expectedStr: "x y z w"}, // Четыре слова, по одному пробелу

		test{words: []string{"x", "y"}, stringLength: 4, expectedStr: "x  y"},                  // Два слова, два пробела
		test{words: []string{"x", "y", "z"}, stringLength: 7, expectedStr: "x  y  z"},          // Три слова, по два пробела
		test{words: []string{"x", "y", "z"}, stringLength: 13, expectedStr: "x     y     z"},   // Три слова, по пять пробелов
		test{words: []string{"x", "y", "z", "w"}, stringLength: 10, expectedStr: "x  y  z  w"}, // Четыре слова, по два пробела

		test{words: []string{"x", "y"}, stringLength: 5, expectedStr: "x   y"},                    // Два слова, три пробела
		test{words: []string{"x", "y", "z"}, stringLength: 9, expectedStr: "x   y   z"},           // Три слова, по три пробела
		test{words: []string{"x", "y", "z", "w"}, stringLength: 13, expectedStr: "x   y   z   w"}, // Четыре слова, по три пробела

		test{words: []string{"x", "y", "z"}, stringLength: 8, expectedStr: "x   y  z"},           // Три слова, по два+ пробела
		test{words: []string{"x", "y", "z", "w"}, stringLength: 11, expectedStr: "x   y  z  w"},  // Четыре слова, по два+ пробела
		test{words: []string{"x", "y", "z", "w"}, stringLength: 12, expectedStr: "x   y   z  w"}, // Четыре слова, по два++ пробела

		test{words: []string{"xxxx", "yyy", "zzzzz", "ww"}, stringLength: 14 + 3, expectedStr: "xxxx yyy zzzzz ww"},       // Разные слова, по одному пробелу
		test{words: []string{"xxxx", "yyy", "zzzzz", "ww"}, stringLength: 14 + 4, expectedStr: "xxxx  yyy zzzzz ww"},      // Разные слова, по одному+ пробелу
		test{words: []string{"xxxx", "yyy", "zzzzz", "ww"}, stringLength: 14 + 5, expectedStr: "xxxx  yyy  zzzzz ww"},     // Разные слова, по одному++ пробелу
		test{words: []string{"xxxx", "yyy", "zzzzz", "ww"}, stringLength: 14 + 6, expectedStr: "xxxx  yyy  zzzzz  ww"},    // Разные слова, по два пробела
		test{words: []string{"xxxx", "yyy", "zzzzz", "ww"}, stringLength: 14 + 7, expectedStr: "xxxx   yyy  zzzzz  ww"},   // Разные слова, по два+ пробела
		test{words: []string{"xxxx", "yyy", "zzzzz", "ww"}, stringLength: 14 + 8, expectedStr: "xxxx   yyy   zzzzz  ww"},  // Разные слова, по два++ пробела
		test{words: []string{"xxxx", "yyy", "zzzzz", "ww"}, stringLength: 14 + 9, expectedStr: "xxxx   yyy   zzzzz   ww"}, // Разные слова, по три пробела

		test{words: []string{"xxx", "xxx", "xxx", "xxx"}, stringLength: 12 + 3, expectedStr: "xxx xxx xxx xxx"},   // одинаковые слова xxx
		test{words: []string{"xxx", "xxx", "yyy", "yyy"}, stringLength: 12 + 3, expectedStr: "xxx xxx yyy yyy"},   // одинаковые слова xxx yyy
		test{words: []string{"xxx", "xxx", "xxx", "xxx"}, stringLength: 12 + 5, expectedStr: "xxx  xxx  xxx xxx"}, // одинаковые слова xxx неравномерно
		test{words: []string{"xxx", "xxx", "yyy", "yyy"}, stringLength: 12 + 5, expectedStr: "xxx  xxx  yyy yyy"}, // одинаковые слова xxx yyy неравномерно

		test{words: []string{"абвг", "йцу", "ппппп", "ха"}, stringLength: 14 + 3, expectedStr: "абвг йцу ппппп ха"}, // Еще один тест про разные слова, по одному пробелу
	}

	for _, testCase := range testCases {

		actualStr, actualErr := justify(testCase.words, testCase.stringLength)
		fmt.Printf("%#v\n", actualStr)
		if actualErr != nil {
			t.Errorf("Error does not expected but found %v", actualErr)
		}
		if testCase.expectedStr != actualStr {
			t.Errorf("Expected string result '%v', but actual result is '%v'", testCase.expectedStr, actualStr)
		}
	}

}
