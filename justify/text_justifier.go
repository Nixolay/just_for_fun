package justifier

import (
	"errors"
)

/* Задача:
 * Необходимо реализовать выравнивание текста по ширине в методе justify
 *
 * Выравнивание строки по ширине достигается расстановкой пробелов между словами по следующим правилам:
 * - Между каждым словом должен стоять хотя бы один пробел
 * - Пробелы в начале строки должны отсутствовать
 * - Пробелы в конце строки должны отсутствовать
 *      Исключение: строка из одного слова добивается пробелами до конца
 *      пример: "a    "
 * - Пробелы распределяются равномерно
 *      пример: "a  b  c  d  e"
 * - Если равномерное распределение пробелов невозможно,
 *      то "лишние" пробелы добавляются по одному в интервалы начиная с крайнего левого
 *      пример: "a   b   c  d  e"
 *
 * Входные данные:
 * - текст - массив слов, состоящий из латинских букв и цифр, БЕЗ пробельных символов, БЕЗ переносов строк
 * - длина результирующей строки - целое неотрицательное число,
 *      достаточно большое, чтобы между каждым словом в результирующей строке вместилось хотя бы по одному пробелу
 *
 * Пример использования:
 * var result = justify([]string{"xxxx", "yyy", "zzzzz", "ww"}, 20);
 *
 * Для проверки решения необходимо запустить тесты.
 */

func justify(x []string, l int) (out string, err error) {

	if len(x) < 1 {
		err = errors.New("no data")
		return
	}

	if l < 0 {
		err = errors.New("incorrect length")
		return
	}

	tmp := make([]rune, 0, l)

	if len(x) == 1 {
		out = x[0]
		for i := l - len(out); i > 0; i-- {
			out += " "
		}

		return
	}



	lenWords := 0

	for _, v := range x {
		lenWords += len([]rune(v))
	}


	cWord := make([]rune,0,(l - lenWords) / (len(x)-1))
	//bdr := new(strings.Builder)

	if (l - lenWords) == len(x)-1 {
		cWord = append(cWord, []rune(" ")...)
	} else if (l - lenWords) > len(x)-1 {

		for i:=(l - lenWords) / (len(x)-1);i>0;i--{
			//bdr.Write([]byte(" "))
			cWord = append(cWord, []rune(" ")...)
		}
		//cWord = bdr.String()
		//bdr.Reset()
	}


	t := l - (((l -lenWords) / (len(x)-1)) * (len(x)-1) + lenWords)

	for i, v := range x {

		if i == len(x)-1 {
			//out += v
			//bdr.WriteString(v)
			tmp = append(tmp, []rune(v)...)
			break
		}

		//out += v + cWord
		//bdr.WriteString(v + cWord)
		tmp = append(tmp, []rune(v)...)
		tmp = append(tmp, cWord...)

		if t > 0 {
			t--
			//out += " "
			//bdr.Write([]byte(" "))
			tmp = append(tmp, []rune(" ")...)
		}

	}

	out = string(tmp)

	return
}
