package main

import (
	"os/exec"
	"strings"
)

func main() {
	out, err := Command("timedatectl | grep local")
	if err != nil {
		panic(err)
	}

	if !strings.Contains(string(out), "RTC in local TZ: no") {
		println("it's all OK")
		return
	}

	_, err = Command("timedatectl set-local-rtc 1")
	if err != nil {
		panic(err)
	}

	println("submit local rtc")
}

func Command(command string) (string, error) {
	out, err := exec.Command("sh", "-c", command).Output()
	return string(out), err
}
