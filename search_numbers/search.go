package search

import (
	"errors"
)

func SearchNumbers(L []int, s int) (o1, o2 int, err error) {
	tmp := 0
	//point:
	for i, v := range L {
		switch {
		case i == 0:
			tmp = v
		case tmp > v:
			err = errors.New("range is not sorted")
			//break point
			return
		default:
			tmp = v
		}
	}

	tmp, t1, t2 := 0, 0, 0

	for i, v := range L {
		switch i {
		case 0:
			t1 = v
			continue
		case 1:
			t2 = v
			tmp = t1 + t2
			continue
		}

		if ABS(tmp-s) < ABS((t2+v)-s) {
			return t1, t2, nil
		} else {
			t1, t2 = t2, v
		}

	}

	return
}

func ABS(n int) int {
	if n < 0 {
		return n * -1
	}
	return n
}
