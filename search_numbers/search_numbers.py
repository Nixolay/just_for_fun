import sys
"""
Задача

Дана последовательность натуральных чисел L и число s.
L отсортирована по возрастанию.
Найти два числа l1, l2 из L, сумма которых (l1 + l2) максимально близка к s.

На что обратить внимание:

- Приложение должно поддерживать CLI интерфейс:

~$ app.py 1 3 4 16 17 -s 6
~$ 3 4

где L=[1, 3, 4, 16, 17], s=6, ответ 3 и 4

- Приложение должно обрабатывать исключения связанные с некорректными входными
данными
- Код должен соответствовать рекомендациям по оформлению кода pep8, иметь
логичное выделение и именование сущностей
- Наличие модульных тестов
- setup.py для сборки пакета и развёртывания в среде ОС
- Предложить решение с наилучшей вычислительной сложность.
Подсказка: O(n2) - плохой вариант, O(n) - наилучший

"""


def searchNumbers(L, s):
    if all(b >= a for a, b in zip(L, L[1:])):
        tmp, t1, t2 = None, None, None

        for v in L:
            if t1 is None:
                t1 = v
                continue

            elif t2 is None:
                t2 = v
                tmp = t1 + t2
                continue

            if abs(tmp - s) < (t2 + v) - s:
                return t1, t2

            else:
                t1, t2 = t2, v

    else:
        raise Exception("range is not sorted: {}".format(str(L)))


def main(argv):
    spliter = argv.index("-s")
    L = []

    for v in argv[1:spliter]:
        L.append(int(v))

    print(searchNumbers(L, int(argv[spliter + 1])))


if __name__ == "__main__":
    main(sys.argv)
