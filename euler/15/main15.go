/*
    Начиная в левом верхнем углу сетки 2×2 и имея возможность двигаться только
вниз или вправо, существует ровно 6 маршрутов до правого нижнего угла сетки.
    Сколько существует таких маршрутов в сетке 20×20?
*/
package main

const lGrid = 20

func main() {
	edge := "_"
	//edgeB := "="
	//arrow := ">"
	for i := range [lGrid]int{} {
		switch {
		case i%2 != 0:
			for range [lGrid]int{} {
				print(edge)
			}
			println()
			for j := range [lGrid]int{} {
				switch {
				case j%2 != 0:
					print("| ")
				}
			}
			println("|")
		}
	}

}
