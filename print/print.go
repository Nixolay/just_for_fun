package myprint

type String interface {
	String() string
}

func Println(values ...interface{}) {

	for _, v := range values {
		switch v.(type) {
		case int:
			print(v.(int))
		case float32:
			print(v.(float32))
		case string:
			print(v.(string))
		case bool:
			print(v.(bool))
		default:
			print(v.(String).String())
		}

		print(" ")
	}

	println()
}
