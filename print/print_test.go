package myprint

import (
	"testing"
)

type MyPrint struct {
	data string
}

func (mp MyPrint) String() string {
	return mp.data
}

func TestHelloWorld(t *testing.T) {
	defer func() {
		if err := recover(); err != nil {
			t.Fatal(err)
		}
	}()

	mp := MyPrint{data: "hello"}
	Println(mp, 1, "str", true, mp, &mp)
}
